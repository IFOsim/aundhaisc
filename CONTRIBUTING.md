How to contribute:

* all of the python code / notebooks should be using Python3.6+; support for python 2.6/2.7 is gone

* learn how to use GIT on the command line; if you are using Windows or are not yet comfortable with the GIT command line, GitKracken is a good GUI tool for GIT version control interfacing.

* GIT is NOT like Dropbox. This is not meant to hold large binary files. Do not commit your plots and data files here. 

* For Jupyter notebooks, please Clear All Outputs before saving and committing.

* please add README for each directory with an explanation of what each code is.

* put lots of comments in your code so that new people can understand it. Its important for us to have many useful examples.

* for Finesse/PyKat questions please use the Finesse chat channel in chat.ligo.org