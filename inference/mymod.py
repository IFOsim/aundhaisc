# This module contains all the functions for calculating probablity  
# model Function, Loglikelyhood function, logPrior function, log posterior funcion 
# Any other function which is not part of main program can also be added here 


# Data(E): The observed data which is used as evidence to calculate the range of possible parameters. 
# Theta(θ): bundle of parameters 



# Description of functions 
# model Function: It returns the signal values for the given parameters(θ)  
# LogLikelyhood function: Takes theta(θ) and observed data(E) as input and caluclates the log likelyhood i.e. log(P(E|θ))
# LogPrior function: calculates the log of prior know probablity of the parameters. i.e. log(P(θ))
# LogPosterior function : calulates the log of posterior prabablity of parameters.   i.e. log (P(E|θ)*P(θ))



# Import as:    from mymod import mymod, modelFunction, lnlike, lnprior, lnpost
# As the model function invariably requires a pykat object. The object should be assisned prior to using any of the functions. i.e. mymod.obname=ob  



# Future Work
# 1. Generalizing the function-currently each of the function takes specific number of parameter














import numpy as np
global obname   # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global lnpriorlims
def modelFunction(theta):
    T1,T2=theta
#    obname=Baseob.deepcopy()
    obname.ITMX.R=1-T1
    obname.ITMX.T=T1
    obname.ETMX.R=1-T2
    obname.ETMX.T=T2
    try:
        res=obname.run()
        return res['P_Trans']
    except:
        print("An exception occurred")
        print([r1,r2])
        return np.inf*np.ones((1001,))
            

def lnlike(theta, data):
    model = modelFunction(theta)
    return -0.5 * np.sum((data - model)**2)    

def lnprior(theta,lnpriorlims):
    T1,T2=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpost(theta,data):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlike(theta,data) + lnprior(theta,lnpriorlims)





# ------------------------------For PDH -------

def modelFunctionPDH(theta):
    le,df=theta
#    obname=Baseob.deepcopy()
    obname.lp.L=le
    obname.eo1.f=df
    obname.REFLI.f1=df
    obname.REFLQ.f1=df
    
    try:
        res=obname.run()
        # print("te=",res['te'])
        return res['my']
    except:
        print("An exception occurred")
        return np.inf*np.ones((1001,))



def lnlikePDH(theta, data):
    model = modelFunctionPDH(theta)
    return -0.5 * np.sum((data - model)**2)    

def lnprior(theta,lnpriorlims):
    T1,T2=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpostPDH(theta,data):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlikePDH(theta,data) + lnprior(theta,lnpriorlims)        



# ------------------------------For TF -------

def modelFunctionTF(theta):
    le,df=theta
#    obname=Baseob.deepcopy()
    obname.lp.L=le
    obname.eo1.f=df
    obname.inphase.f1=df
    
    try:
        res=obname.run()
        # print("te=",res['te'])
        return res['inphase']
    except Exception as e:
        print("An exception occurred")
        print(e)
        return np.inf*np.ones((1001,))

def lnlikeTF(theta, data):
    model = modelFunctionTF(theta)
    return -0.5 * np.sum((data - model)**2)    

def lnprior(theta,lnpriorlims):
    T1,T2=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpostTF(theta,data):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlikeTF(theta,data) + lnprior(theta,lnpriorlims)        



# ------------------------------For TFTT -------

def modelFunctionTFTT(theta):
    T1,T2=theta
#    obname=Baseob.deepcopy()
    obname.ITMX.R=1-T1
    obname.ITMX.T=T1
    obname.ETMX.R=1-T2
    obname.ETMX.T=T2
    try:
        res=obname.run()
        # print("te=",res['te'])
        return res['inphase']
    except Exception as e:
        print("An exception occurred")
        print(e)
        return np.inf*np.ones((1001,))


def lnlikeTFTT(theta, data):
    model = modelFunctionTFTT(theta)
    return -0.5 * np.sum((data - model)**2)    

def lnprior(theta,lnpriorlims):
    T1,T2=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpostTFTT(theta,data):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlikeTFTT(theta,data) + lnprior(theta,lnpriorlims) 



# ------------------------------For TFTTLDF -------

def modelFunctionTFTTLDf(theta):
    T1,T2,le,df=theta
#    obname=Baseob.deepcopy()
    obname.ITMX.R=1-T1
    obname.ITMX.T=T1
    obname.ETMX.R=1-T2
    obname.ETMX.T=T2
    obname.lp.L=le
    obname.eo1.f=df
    obname.inphase.f1=df
    try:
        res=obname.run()
        # print("te=",res['te'])
        return res['inphase']
    except Exception as e:
        print("An exception occurred")
        print(e)
        return np.inf*np.ones((1001,))


def lnlikeTFTTLDf(theta, data):
    model = modelFunctionTFTTLDf(theta)
    return -0.5 * np.sum((data - model)**2)    

def lnpriorTFTTLDf(theta,lnpriorlims):
    T1,T2,le,df=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] and  lnpriorlims[4] < le < lnpriorlims[5] and  lnpriorlims[6] < df < lnpriorlims[7] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpostTFTTLDf(theta,data):
    if not np.isfinite(lnpriorTFTTLDf(theta,lnpriorlims)):
        return -np.inf
    return lnlikeTFTTLDf(theta,data) + lnpriorTFTTLDf(theta,lnpriorlims) 
