import pykat
import numpy as np
import matplotlib.pyplot as plt
from pykat.optics.maps import surfacemap
pykat.init_pykat_plotting()


N = 200
radius = 0.17
dx = 2*radius/N


itm_map = surfacemap(
    "itm_map",
    "phase reflection",
    size=(N,N),
    scaling=1,
    step_size=(dx, dx),
    center=((N-1)/2, (N-1)/2)
)
X, Y = np.meshgrid(itm_map.x, itm_map.y)
# Nanorad tilt
itm_map.data = 1e-9 * X # Units of `smap.scaling` which is set to meter above
itm_map.plot();
itm_map.write_map("itm_map.txt")


etm_map = surfacemap(
    "etm_map",
    "phase reflection",
    size=(N,N),
    scaling=1,
    step_size=(dx, dx),
    center=((N-1)/2, (N-1)/2)
)
X, Y = np.meshgrid(etm_map.x, etm_map.y)
etm_map.data[:] = 0
etm_map.plot();
etm_map.write_map("etm_map.txt")


amap = surfacemap(
    "aperture_map",
    "absorption both",
    size=(N,N),
    step_size=(dx, dx),
    center=((N-1)/2, (N-1)/2)
)
R = np.sqrt(X**2 + Y**2)
amap.data[R > radius] = 1 # absorb everything outside mirror
amap.data[R <= radius] = 0
amap.write_map("aperture_map.txt")
amap.plot();



kat = pykat.finesse.kat()
kat.verbose = False
kat.parse(f"""
maxtem 2

l i1 1 0 0 nITM1
m mITM 0.986 0.014 0 nITM1 nITM2
s sC 3994 nITM2 nETM1
m mETM 1 0 0 nETM1 nETM2

attr mITM Rc -1934
attr mETM Rc 2245

cav cav_mode mITM nITM2 mETM nETM1

map mITM itm_map.txt
map mITM aperture_map.txt
map mETM etm_map.txt
map mETM aperture_map.txt

# Faster integration method
conf mITM integration_method 4
conf mETM integration_method 4

ad a00 0 0 0 nETM1
ad a10 1 0 0 nETM1
ad a01 0 1 0 nETM1
ad a20 2 0 0 nETM1
ad a02 0 2 0 nETM1
    
xaxis mETM phi lin 0 180 1000
yaxis log abs
""")

out = kat.run()

plt.figure()
for ad in kat.getAll(pykat.detectors.ad):
    plt.semilogy(out.x, abs(out[ad.name])**2, label=f'{ad.n}{ad.m}')
    
P_total = np.sum(abs(out[(ad.name for ad in kat.getAll(pykat.detectors.ad))])**2, 0)
plt.semilogy(out.x, P_total, ls='--', c='k', lw=2, label='Total')

plt.legend()
plt.ylabel("Power [W]")
plt.xlabel("Tuning [deg]")
plt.show()
