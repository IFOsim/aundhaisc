import numpy as np
global obname1  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global obname2  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global lnpriorlims
global globdata
global globvariance


def modelFunction(theta):
    T1,T2,le,phi,phitf=theta

    obname1.ITMX.T=T1
    obname1.ITMX.R=1-T1-obname1.ITMX.L
    obname1.ETMX.T=T2
    obname1.ETMX.R=1-T2-obname1.ETMX.L
    obname1.LX.L=le
    obname1.REFLI.phase1=phi
    obname1.REFLQ.phase1=phi+90


    obname2.ITMX.T=T1
    obname2.ITMX.R=1-T1-obname1.ITMX.L
    obname2.ETMX.T=T2
    obname2.ETMX.R=1-T2-obname1.ETMX.L
    obname2.LX.L=le
    obname2.TF_REFLI.phase1=phi
    obname2.TF_REFLQ.phase1=phi+90
    obname2.TF_REFLI.phase2=phitf
    obname2.TF_REFLQ.phase2=phitf+90

    try:
        res1=obname1.run()    
        res2=obname2.run()     
        return np.concatenate([res1['P_Trans'], res1['REFLI'], res1['REFLQ'] ,res2['TF_REFLI'], res2['TF_REFLQ']])
 
    except Exception as e:
        print("An exception occurred")
        
        


def lnlike(theta):
    model = modelFunction(theta)
    return -0.5 * np.sum((np.divide(globdata - model,globvariance)**2)[:])    
    # return -0.5 * np.sum(((globdata - model)**2)[:])    

def lnprior(theta,lnpriorlims):
    T1,T2,le,phi,phitf=theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] and  lnpriorlims[4] < le < lnpriorlims[5] and  lnpriorlims[6] < phi < lnpriorlims[7] and  lnpriorlims[8] < phitf < lnpriorlims[9]  :
        return 0.0
    return -np.inf  

def lnpost(theta):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlike(theta) + lnprior(theta,lnpriorlims) 




def getShotNoise(theta):
    T1,T2,le,phi,phitf=theta

    obname1.ITMX.T=T1
    obname1.ITMX.R=1-T1-obname1.ITMX.L
    obname1.ETMX.T=T2
    obname1.ETMX.R=1-T2-obname1.ETMX.L
    obname1.LX.L=le
    obname1.REFLI.phase1=phi
    obname1.REFLQ.phase1=phi+90
    obname1.REFLI_shot.phase1=phi
    obname1.REFLQ_shot.phase1=phi+90

    obname2.ITMX.T=T1
    obname2.ITMX.R=1-T1-obname1.ITMX.L
    obname2.ETMX.T=T2
    obname2.ETMX.R=1-T2-obname1.ETMX.L
    obname2.LX.L=le
    obname2.TF_REFLI.phase1=phi
    obname2.TF_REFLQ.phase1=phi+90
    obname2.TF_REFLI.phase2=phitf
    obname2.TF_REFLQ.phase2=phitf+90
    obname2.TF_REFLI_shot.phase1=phi
    obname2.TF_REFLQ_shot.phase1=phi+90
    obname2.TF_REFLI_shot.phase2=phitf
    obname2.TF_REFLQ_shot.phase2=phitf+90

    try:
        res1=obname1.run()    
        res2=obname2.run()     
        return np.concatenate([res1['P_Trans_shot'], res1['REFLI_shot'], res1['REFLQ_shot'] ,res2['TF_REFLI_shot'], res2['TF_REFLQ_shot']])
 
    except Exception as e:
        print("An exception occurred")