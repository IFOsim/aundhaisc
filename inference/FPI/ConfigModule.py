global FPIincode, FPIDict, PRMIincode, PRMIDict

import pykat.ifo.aligo as aligo
base = aligo.make_kat()
# print(base)


##################    Fabry Perot Interferometer(FPI)   ##################


##   Dictionary containing the key value pairs of the variables
FPIDict={
'mod_f': base.mod1.f.value,
'T_itmx_act': base.ITMX.T.value,
'Loss_itmx_act': base.ITMX.L.value,
'T_etmx_act': base.ETMX.T.value,
'Loss_etmx_act': base.ETMX.L.value,	 
'LX_act': base.LX.L.value,	 
}


# print(FPIDict)


FPIincode= """ 

l L0 125.0 0.0 0.0 ni
s lmod1 1.0 ni n1
mod mod1 {mod_f} 0.18 1 pm 0.0 n1 nLaserOut
s lx1 4.993 nLaserOut nITMX1
m1 ITMX {T_itmx_act} {Loss_itmx_act} 0.0 nITMX1 nITMX2
s LX {LX_act} nITMX2 nETMX1
m1 ETMX {T_etmx_act} {Loss_etmx_act} 0.0 nETMX1 nETMX2


""".format(**FPIDict)


# print(FPIincode)



##################    Power Recycled Michelson Interferometer(PRMI)   ##################


##   Dictionary containing the key value pairs of the variables
PRMIDict={
'mod_f': base.mod1.f.value,
'T_prm_act': base.PRM.T.value,
'Loss_prm_act': base.PRM.L.value,
'T_itmx_act': base.ITMX.T.value,	 
'Loss_itmx_act': base.ITMX.L.value,	 
'T_itmy_act': base.ITMY.T.value,	 
'Loss_itmy_act': base.ITMY.L.value,	 
'lp_act': base.lp1.L.value+base.lp2.L.value+base.lp3.L.value,	 
'ly_act': base.ly1.L.value,	 
'lx_act': base.lx1.L.value,	 
}
# print(PRMIDict)


PRMIincode= """ 

l L0 125.0 0.0 0.0 ni
s lmod1 1.0 ni n1
mod mod1 {mod_f} 0.18 1 pm 0.0 n1 nLaserOut
s sPRCin 0.4135 nLaserOut nPRM1
m1 PRM {T_prm_act} {Loss_prm_act} 0.0 nPRM1 nPRM2
s lpt {lp_act} nPRM2 nPRBS                                             #### lpt =lp1+lp2+lp3

bs1 BS 0.5 3.75e-05 0.0 45.0 nPRBS nYBS nXBS nSRBS


s ly1 {ly_act} nYBS nITMY
m1 ITMY {T_itmy_act} {Loss_itmy_act} 0.0 nITMY nITMY2

s lx1 {lx_act} nXBS nITMX
m1 ITMX {T_itmx_act} {Loss_itmx_act} 0.0 nITMX nITMX2

""".format(**PRMIDict)


# print(PRMIincode)



##########################   Aligo parameters

# %%% FTblock laser
# l L0 125.0 0.0 0.0 ni
# bs jitter 1.0 0.0 0.0 0.0 ni n0 dump dump
# s lmod1 1.0 n0 n1
# mod mod1 9099471.0 0.18 1 pm 0.0 n1 n2
# s lmod2 1.0 n2 n3
# mod mod2 45497355.0 0.18 1 pm 0.0 n3 nLaserOut
# %%% FTend laser

# %%% FTblock IMC
# s sIMCin 0.0 nLaserOut nMC1in
# bs1 MC1 0.006 0.0 0.0 44.59 nMC1in nMC1refl nMC1trans nMC1fromMC3
# s sMC1_MC2 16.24057 nMC1trans nMC2in
# bs1 MC2 0.0 0.0 0.0 0.82 nMC2in nMC2refl nMC2trans dump
# attr MC2 Rcx 27.24
# attr MC2 Rcy 27.24
# s sMC2_MC3 16.24057 nMC2refl nMC3in
# bs1 MC3 0.006 0.0 0.0 44.59 nMC3in nMC3refl nMC3trans nMCreturn_refl
# s sMC3substrate 0.0845 1.44963098985906 nMC3trans nMC3ARin
# bs2 MC3AR 0.0 0.0 0.0 28.9661 nMC3ARin dump nIMCout dump
# s sMC3_MC1 0.465 nMC3refl nMC1fromMC3
# %%% FTend IMC

# %%% FTblock HAM2
# s sHAM2in 0.4282 nIMCout nIM11
# bs1 IM1 0.0 0.0 0.0 53.0 nIM11 nIM12 dump dump
# s sIM1_IM2 1.2938 nIM12 nIM21
# bs1 IM2 0.0 0.0 0.0 7.0 nIM21 nIM22 dump dump
# attr IM2 Rcx 12.8
# attr IM2 Rcy 12.8
# s sIM2_FI 0.26 nIM22 nFI1
# dbs FI nFI1 nFI2 nFI3 nREFL
# s sFI_IM3 0.91 nFI3 nIM31
# bs1 IM3 0.0 0.0 0.0 7.1 nIM31 nIM32 dump dump
# attr IM3 Rcx -6.24
# attr IM3 Rcy -6.24
# s sIM3_IM4 1.21 nIM32 nIM41
# bs1 IM4 0.0 0.0 0.0 45.0 nIM41 nHAM2out dump dump
# %%% FTend HAM2

# %%% FTblock PRC
# s sPRCin 0.4135 nHAM2out nPRM1
# m2 PRMAR 0.0 4e-05 0.0 nPRM1 nPRMs1
# s sPRMsub1 0.0737 1.44963098985906 nPRMs1 nPRMs2
# m1 PRM 0.03 8.5e-06 0.0 nPRMs2 nPRM2
# attr PRM Rcx 11.009
# attr PRM Rcy 11.009
# s lp1 16.6107 nPRM2 nPR2a
# bs1 PR2 0.00025 3.75e-05 0.0 -0.79 nPR2a nPR2b nPOP nAPOP
# attr PR2 Rcx -4.545
# attr PR2 Rcy -4.545
# s lp2 16.1647 nPR2b nPR3a
# bs1 PR3 0.0 3.75e-05 0.0 0.615 nPR3a nPR3b dump dump
# attr PR3 Rcx 36.027
# attr PR3 Rcy 36.027
# s lp3 19.5381 nPR3b nPRBS
# %%% FTend PRC

# %%% FTblock BS
# bs1 BS 0.5 3.75e-05 0.0 45.0 nPRBS nYBS nBSi1 nBSi3
# s BSsub1 0.0687 1.44963098985906 nBSi1 nBSi2
# s BSsub2 0.0687 1.44963098985906 nBSi3 nBSi4
# bs2 BSAR1 5e-05 0.0 0.0 -29.195 nBSi2 dump14 nXBS nPOX
# bs2 BSAR2 5e-05 0.0 0.0 29.195 nBSi4 dump15 nSRBS dump16
# %%% FTend BS

# %%% FTblock Yarm
# s ly1 5.0126 nYBS nITMY1a
# lens ITMY_lens 34500.0 nITMY1a nITMY1b
# s sITMY_th2 0.0 nITMY1b nITMY1
# m2 ITMYAR 0.0 2e-05 0.0 nITMY1 nITMYs1
# s ITMYsub 0.2 1.44963098985906 nITMYs1 nITMYs2
# m1 ITMY 0.014 3.75e-05 0.0 nITMYs2 nITMY2
# attr ITMY Rcx -1934.0
# attr ITMY Rcy -1934.0
# attr ITMY mass 40.0
# s LY 3994.4692 nITMY2 nETMY1
# m1 ETMY 5e-06 3.75e-05 0.0 nETMY1 nETMYs1
# attr ETMY Rcx 2245.0
# attr ETMY Rcy 2245.0
# attr ETMY mass 40.0
# s ETMYsub 0.2 1.44963098985906 nETMYs1 nETMYs2
# m2 ETMYAR 0.0 0.0005 0.0 nETMYs2 nPTY
# %%% FTend Yarm

# %%% FTblock Xarm
# s lx1 4.993 nXBS nITMX1a
# lens ITMX_lens 34500.0 nITMX1a nITMX1b
# s sITMX_th2 0.0 nITMX1b nITMX1
# m2 ITMXAR 0.0 2e-05 0.0 nITMX1 nITMXs1
# s ITMXsub 0.2 1.44963098985906 nITMXs1 nITMXs2
# m1 ITMX 0.014 3.75e-05 0.0 nITMXs2 nITMX2
# attr ITMX Rcx -1934.0
# attr ITMX Rcy -1934.0
# attr ITMX mass 40.0
# s LX 3994.4692 nITMX2 nETMX1
# m1 ETMX 5e-06 3.75e-05 0.0 nETMX1 nETMXs1
# attr ETMX Rcx 2245.0
# attr ETMX Rcy 2245.0
# attr ETMX mass 40.0
# s ETMXsub 0.2 1.44963098985906 nETMXs1 nETMXs2
# m2 ETMXAR 0.0 0.0005 0.0 nETMXs2 nPTX
# %%% FTend Xarm

# %%% FTblock SRC
# s ls3 19.3661 nSRBS nSR3b
# bs1 SR3 0.0 3.75e-05 0.0 0.785 nSR3b nSR3a dump dump
# attr SR3 Rcx 35.972841
# attr SR3 Rcy 35.972841
# s ls2 15.4435 nSR3a nSR2b
# bs1 SR2 0.0 3.75e-05 0.0 -0.87 nSR2b nSR2a dump dump
# attr SR2 Rcx -6.406
# attr SR2 Rcy -6.406
# s ls1 15.7586 nSR2a nSRM1
# m1 SRM 0.2 8.7e-06 90.0 nSRM1 nSRMs1
# attr SRM Rcx -5.6938
# attr SRM Rcy -5.6938
# s SRMsub 0.0749 1.44963098985906 nSRMs1 nSRMs2
# m2 SRMAR 0.0 5e-08 0.0 nSRMs2 nSRM2
# s sSRM_FI 0.7278 nSRM2 nFI2a
# %%% FTend SRC

# %%% FTblock FI
# dbs FI2 nFI2a nFI2b nFI2c nFI2d
# s sFI_OM1 2.9339 nFI2c nOM1a
# bs1 OM1 0.0008 3.75e-05 0.0 2.251 nOM1a nOM1b nOM1c dump22
# attr OM1 Rcx 4.6
# attr OM1 Rcy 4.6
# s sOM1_OM2 1.395 nOM1b nOM2a
# bs1 OM2 1e-05 3.75e-05 0.0 4.399 nOM2a nOM2b nOM2c nOM2d
# attr OM2 Rcx 1.7058
# attr OM2 Rcy 1.7058
# s sOM2_OM3 0.631 nOM2b nOM3a
# bs1 OM3 1e-05 3.75e-05 0.0 30.037 nOM3a nOM3b nOM3c nOM3d
# s sOM3_OMC 0.2034 nOM3b nOMC_ICa
# %%% FTend FI

# %%% FTblock OMC
# bs1 OMC_IC 0.0076 1e-05 0.0 2.7609 nOMC_ICa nOMC_ICb nOMC_ICc nOMC_ICd
# s lIC_OC 0.2815 nOMC_ICc nOMC_OCa
# bs1 OMC_OC 0.0075 1e-05 0.0 4.004 nOMC_OCa nOMC_OCb nAS nOMC_OCd
# s lOC_CM1 0.2842 nOMC_OCb nOMC_CM1a
# bs1 OMC_CM1 3.6e-05 1e-05 0.0 4.004 nOMC_CM1a nOMC_CM1b nOMC_CM1c nOMC_CM1d
# attr OMC_CM1 Rcx 2.57321
# attr OMC_CM1 Rcy 2.57321
# s lCM1_CM2 0.2815 nOMC_CM1b nOMC_CM2a
# bs1 OMC_CM2 3.59e-05 1e-05 0.0 4.004 nOMC_CM2a nOMC_CM2b nOMC_CM2c nOMC_CM2d
# attr OMC_CM2 Rcx 2.57369
# attr OMC_CM2 Rcy 2.57369
# s lCM2_IC 0.2842 nOMC_CM2b nOMC_ICd
# %%% FTend OMC

# %%% FTblock cavities
# cav cavIMC MC2 nMC2in MC2 nMC2refl
# cav cavXARM ITMX nITMX2 ETMX nETMX1
# cav cavYARM ITMY nITMY2 ETMY nETMY1
# cav cavSRX SRM nSRM1 ITMX nITMXs2
# cav cavSRY SRM nSRM1 ITMY nITMYs2
# cav cavPRX PRM nPRM2 ITMX nITMXs2
# cav cavPRY PRM nPRM2 ITMY nITMYs2
# cav cavOMC OMC_IC nOMC_ICc OMC_IC nOMC_ICd
# %%% FTend cavities
# yaxis abs