import numpy as np
global obname1  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global obname2  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global lnpriorlims
global globdata
global globvariance


def modelFunction(theta):
    T_itmx,T_itmy,T_prm,lx,ly,lp=theta

    obname1.ITMX.T=T_itmx
    obname1.ITMX.R=1-T_itmx-obname1.ITMX.L
    obname1.ITMY.T=T_itmy
    obname1.ITMY.R=1-T_itmy-obname1.ITMY.L
    obname1.PRM.T=T_prm
    obname1.PRM.R=1-T_prm-obname1.PRM.L
    
    obname1.lx1.L=lx
    obname1.ly1.L=ly
    obname1.lpt.L=lp

#     obname2.ITMX.T=T_itmx
#     obname2.ITMX.R=1-T_itmx-obname2.ITMX.L
#     obname2.ITMY.T=T_itmy
#     obname2.ITMY.R=1-T_itmy-obname2.ITMY.L
#     obname2.PRM.T=T_prm
#     obname2.PRM.R=1-T_prm-obname2.PRM.L
    
#     obname2.lx1.L=lx
#     obname2.ly1.L=ly
#     obname2.lpt.L=lp


    try:
        res1=obname1.run()    
#         res2=obname2.run()
        return np.concatenate([res1['P_Trans'], res1['P_POP'], res1['REFLI'] ,res1['REFLQ'], res1['POPI'], res1['POPQ']])
#         return np.concatenate([res1['P_Trans'], res1['P_POP'], res1['REFLI'] ,res1['REFLQ'], res1['POPI'], res1['POPQ'],    res2['P_Trans'], res2['P_POP'], res2['REFLI'] ,res2['REFLQ'], res2['POPI'], res2['POPQ'] ])
#         return np.concatenate([res1['P_Trans'], res1['REFLI'], res1['REFLQ'] ,res2['TF_REFLI'], res2['TF_REFLQ']])
 
    except Exception as e:
        print("An exception occurred")
        
        


def lnlike(theta):
    model = modelFunction(theta)
    return -0.5 * np.sum((np.divide(globdata - model,globvariance)**2)[:])    
    # return -0.5 * np.sum(((globdata - model)**2)[:])    

def lnprior(theta,lnpriorlims):
    T_itmx,T_itmy,T_prm,lx,ly,lp=theta
    if lnpriorlims[0] < T_itmx < lnpriorlims[1] and  lnpriorlims[2] < T_itmy < lnpriorlims[3] and  lnpriorlims[4] < T_prm < lnpriorlims[5] and  lnpriorlims[6] < lx < lnpriorlims[7] and  lnpriorlims[8] < ly < lnpriorlims[9] and  lnpriorlims[10] < lp < lnpriorlims[11]  :
        return 0.0
    return -np.inf  

def lnpost(theta):
    if not np.isfinite(lnprior(theta,lnpriorlims)):
        return -np.inf
    return lnlike(theta) + lnprior(theta,lnpriorlims) 




def getShotNoise(theta):
    T_itmx,T_itmy,T_prm,lx,ly,lp=theta

    obname1.ITMX.T=T_itmx
    obname1.ITMX.R=1-T_itmx-obname1.ITMX.L
    obname1.ITMY.T=T_itmy
    obname1.ITMY.R=1-T_itmy-obname1.ITMY.L
    obname1.PRM.T=T_prm
    obname1.PRM.R=1-T_prm-obname1.PRM.L
    
    obname1.lx1.L=lx
    obname1.ly1.L=ly
    obname1.lpt.L=lp

    try:
        res1=obname1.run()    
#         res2=obname2.run()   
        return np.concatenate([res1['P_Trans_shot'], res1['P_POP_shot'], res1['REFLI_shot'] ,res1['REFLQ_shot'], res1['POPI_shot'], res1['POPQ_shot']])

#         return np.concatenate([res1['P_Trans_shot'], res1['REFLI_shot'], res1['REFLQ_shot'] ,res2['TF_REFLI_shot'], res2['TF_REFLQ_shot']])
 
    except Exception as e:
        print("An exception occurred")