# @Author: sivananda
# @Date:   2021-06-10T17:25:16+05:30
# @Filename: finMCMCmodeller.py
# @Last modified by:   sivananda
# @Last modified time: 2021-08-05T00:05:52+05:30

# TODO: Need to write lots of comments

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
import pykat
import pykat.ifo
import json
import emcee
import corner
import pandas as pd

class sim_obj(object):
    '''
    This object has code for simulation and associated measurement sim_results

    '''
    def __init__(self, sim_name = '',
                 sim_code = '',         #Simulation code
                 n_ports = 1,            #number of ports
                 ports_name_list = [],   #Name of the ports
                 measured_data = [],     #dictonary of measured_data
                 opt_demod_phase = True, #whether to find demod phase
                 xbounds = np.array([-0.01, 0.01])):
        self.sim_name = sim_name;
        self.sim_code = sim_code;
        self.measured_data = measured_data;
        self.opt_demod_phase = opt_demod_phase;
        self.xbounds = xbounds
        self.n_ports = n_ports
        self.ports_name_list = ports_name_list

    def correct_demod_phase(self, cdata, xaxis):
        demod_phase, optical_gain = pykat.ifo.opt_demod_phase(cdata = cdata,
                                                              x = xaxis,
                                                              xbounds = self.xbounds)
        rotated_cdata = cdata*np.e**(-1j*np.radians(demod_phase))
        return rotated_cdata;



class finMCMCmodeller(object):
    """
    This is wrapper class to finesse simulation using pykat suitable for use
    with MCMC analysis. Detailed usage help is at finMCMCmodeller-help.ipynb

    Attributes:
        model_code (str) : kat code of the model that is to be simulated using
        finesse. default is a blank string
        param_name_list (list) : This is a list of names of the parameters that
        are to be varied during the mcmc simulation
        param_limits (dict) : This is dictionary of limits of the parameters in
        the param_name_list. This is used in function 'lnprior()'.
        sim_result_name_list (list) : This takes list of simulation results
        that are to be compared with measurement data
        measured_data (dict) : This is dictonary with keywords given in
        sim_result_name_list. This data from the measurements made from the


    """
    def __init__(self, model_code = '',
                 param_name_list = {},
                 param_limits = {},
                 sim_obj_dict = {}):
        self.model_code = model_code;
        self.param_name_list = list(param_name_list);
        self.param_limits = param_limits;
        self.sim_obj_dict = sim_obj_dict;
        return None;

    def simulate(self, sim_name, param_values):
        '''
        This method simulates the finesse code = (Model Code + respective sim code)
        constants defined in the model code are replaced with param_values

        '''
        kat_obj = pykat.finesse.kat()
        kat_obj.verbose = False;

        #parse the model code
        kat_obj.parse(self.model_code,
                      keepComments=False,
                      preserveConstants=True);

        #update the constants that are to be varied for each run of MCMC
        for item in self.param_name_list:
            kat_obj.constants[item].value = float(param_values[item])

        #parse the associated simulation code
        kat_obj.parse(self.sim_obj_dict[sim_name].sim_code,
                      keepComments=False,
                      preserveConstants=True)

        out = kat_obj.run();
        results = {};
        results['x'] = list(out.x);
        #find the optimium demod phase and rotate the vector.
        if self.sim_obj_dict[sim_name].opt_demod_phase:
            for item in self.sim_obj_dict[sim_name].ports_name_list:
                rotated_cdata = self.sim_obj_dict[sim_name].correct_demod_phase(out[item], out.x)
                results[item] = rotated_cdata;
        else:
            for item in self.sim_obj_dict[sim_name].ports_name_list:
                results[item] = list(out[item])
        return results, out;

    def compare(self, sim_name, param_values):
        '''
        this function will be deleted in future
        '''
        results, out = self.simulate(sim_name, param_values)
        measured_data = self.sim_obj_dict[sim_name].measured_data
        results_interp1d_re = {}
        results_interp1d_im = {}
        results_interp1d = {}
        difference = {}
        difference['x'] = measured_data['x']
        for item in self.sim_obj_dict[sim_name].ports_name_list:
            interp1d_func_re = interp1d(results['x'],
                                        np.real(results[item]))
            interp1d_func_im = interp1d(results['x'],
                                        np.imag(results[item]))
            results_interp1d_re[item] = interp1d_func_re(measured_data['x'])
            results_interp1d_im[item] = interp1d_func_im(measured_data['x'])
            results_interp1d[item] = (results_interp1d_re[item] +
                                      1j*results_interp1d_im[item])
            difference[item] = results_interp1d[item] - measured_data[item]
        return difference

    def compare(self, sim_name, param_values, no_interpolate):
        '''
        This function compares the simulation results with measured data
        '''
        results, out = self.simulate(sim_name, param_values)
        measured_data = self.sim_obj_dict[sim_name].measured_data
        difference = {}
        difference['x'] = measured_data['x']
        for item in self.sim_obj_dict[sim_name].ports_name_list:
            difference[item] = (np.array(results[item]) -
                                np.array(measured_data[item]))
        return difference

    def lnprior(self,theta):
        '''
        prior probablity distribution -
        Here in this is flat within param_limits
        '''
        param_values = {}
        param_limits = self.param_limits
        for i in range(len(theta)):
            param_values[list(param_limits.keys())[i]] = theta[i]
        for item in param_values:
            if not(min(param_limits[item]) < param_values[item] <
                   max(param_limits[item])):
                return -np.inf
        return 0.0;

    def lnlike(self,theta):
        '''
        log likelyhood probablity distribution

        '''
        param_values = {}
        param_limits = self.param_limits
        for i in range(len(theta)):
            param_values[list(param_limits.keys())[i]] = theta[i]

        sum_squares = 0.0
        for item in self.sim_obj_dict:
            diff = self.compare(self.sim_obj_dict[item].sim_name,
                                param_values, no_interpolate=True)
            for i in self.sim_obj_dict[item].ports_name_list:
                sum_squares += np.sum(np.real(diff[i])**2)
        return -0.5*sum_squares;

    def lnpost(self,theta):
        '''
        posterior distribution

        '''
        lnprior_val = self.lnprior(theta)
        if np.isfinite(lnprior_val):
            return self.lnlike(theta) + lnprior_val
        else:
            return lnprior_val

    def gen_init_position(self, nwalkers):
        '''
        generates initial position for the walkers.
        random position is picked with the param_limits
        '''
        param_limits = self.param_limits
        init_pos = []
        for i in range(nwalkers):
            each_pos = []
            for item in param_limits:
                random_n = 0.0
                limits = param_limits[item]
                while random_n == 0.0:
                    random_n = np.random.rand()
                pos = min(limits) + random_n*(max(limits) - min(limits))
                each_pos.append(pos)
            init_pos.append(each_pos)
        self.theta0 = np.array(init_pos)
        return self.theta0

    def gen_parallel_code(self):
        par_code = {}
        par_code['model_code'] = self.model_code;
        par_code['param_name_list'] = self.param_name_list;
        par_code['param_limits'] = self.param_limits;
        par_code['n_sim'] = len(self.sim_obj_dict);
        sim_count = 0
        for key, each_sim_obj in self.sim_obj_dict.items():
            temp_dict = {}
            temp_dict['sim_name'] = each_sim_obj.sim_name;
            temp_dict['sim_code'] = each_sim_obj.sim_code;
            temp_dict['measured_data'] = each_sim_obj.measured_data;
            temp_dict['n_ports'] = each_sim_obj.n_ports;
            temp_dict['ports_name_list'] = each_sim_obj.ports_name_list;
            par_code['sim_' + str(sim_count)] = temp_dict;
            par_code[each_sim_obj.sim_name] = temp_dict;
            sim_count += 1
        self.par_code = par_code
        return self.par_code

    @classmethod
    def par_lnpost(cls, theta, par_code):
        par_lnprior_val = finMCMCmodeller.par_lnprior(theta, par_code)
        if np.isfinite(par_lnprior_val):
            return finMCMCmodeller.par_lnlike(theta, par_code) + par_lnprior_val
        else:
            return par_lnprior_val

    @classmethod
    def par_lnprior(cls, theta, par_code):
        param_values = {}
        param_limits = par_code['param_limits']
        for i in range(len(theta)):
            param_values[list(param_limits.keys())[i]] = theta[i]
        for item in param_values:
            if not(min(param_limits[item]) < param_values[item] <
                   max(param_limits[item])):
                return -np.inf;
        return 0.0;

    @classmethod
    def par_lnlike(cls, theta, par_code):
        param_values = {}
        param_limits = par_code['param_limits']
        for i in range(len(theta)):
            param_values[list(param_limits.keys())[i]] = theta[i]
        sum_squares = 0.0
        for sim_count in range(0,par_code['n_sim']):
            diff = finMCMCmodeller.par_compare(par_code['sim_' + str(sim_count)]['sim_name'],
                                               param_values, no_interpolate=True,
                                               par_code=par_code)
            for i in par_code['sim_' + str(sim_count)]['ports_name_list']:
                sum_squares += np.sum(np.real(diff[i])**2)
        return -0.5*sum_squares;

    @classmethod
    def par_compare(cls, sim_name, param_values, no_interpolate, par_code):
        results, out = finMCMCmodeller.par_simulate(sim_name,
                                                    param_values,
                                                    par_code)
        measured_data = par_code[sim_name]['measured_data']
        difference = {}
        difference['x'] = measured_data['x']
        for item in par_code[sim_name]['ports_name_list']:
            difference[item] = results[item] - measured_data[item]
        return difference

    @classmethod
    def par_simulate(cls, sim_name, param_values, par_code):
        kat_obj = pykat.finesse.kat()
        kat_obj.verbose = False;

        #parse the model code
        kat_obj.parse(par_code['model_code'],
                      keepComments=False,
                      preserveConstants=True);

        #update the constants that are to be varied for each run of MCMC
        for item in par_code['param_name_list']:
            kat_obj.constants[item].value = float(param_values[item])

        #parse the associated simulation code
        kat_obj.parse(par_code[sim_name]['sim_code'],
                      keepComments=False,
                      preserveConstants=True)

        out = kat_obj.run();
        results = {};
        results['x'] = out.x;
        for item in par_code[sim_name]['ports_name_list']:
            results[item] = out[item]
        return results, out;

    def gen_sampler(self,nwalkers,nthreads=1,pool=None):
        ndim = len(self.param_limits)
        self.nwalkers = nwalkers;
        if pool == None:
            self.sampler = emcee.EnsembleSampler(nwalkers=nwalkers,
                                                 ndim= ndim,
                                                 log_prob_fn= finMCMCmodeller.par_lnpost,
                                                 args=(self.par_code,),
                                                 threads= nthreads)
        else:
            self.sampler = emcee.EnsembleSampler(nwalkers=nwalkers,
                                                 ndim= ndim,
                                                 log_prob_fn= finMCMCmodeller.par_lnpost,
                                                 args=(self.par_code,),
                                                 threads= nthreads,
                                                 pool= pool)
        return self.sampler;

    def run_mcmc(self,nsteps,**kwargs):
        self.nsteps = nsteps
        self.sampler.run_mcmc(self.theta0,
                              nsteps, **kwargs)
        self.plotting_ready = True;
        return 0;

    def plot_walkers(self,steps_start=None,
                     steps_stop=None,
                     param_values= None,
                     *args,**kwargs):
        nrows = len(self.param_name_list)
        if self.plotting_ready:
            fig, ax = plt.subplots(nrows= nrows,
                                  ncols= 1,
                                  sharex= True, **kwargs)
            for idx in range(0, nrows):
                for walker in range(0,self.nwalkers):
                    if steps_start == None:
                        steps_start = 0;
                    if steps_stop == None:
                        steps_stop = self.nsteps;
                    x_list = range(steps_start, steps_stop)
                    y_list = list(self.sampler.chain[walker,
                                                     steps_start:steps_stop,
                                                     idx])
                    ax[idx].plot(x_list, y_list);
                    ax[idx].set_ylabel(self.param_name_list[idx])
                if not (param_values == None):
                    key = self.param_name_list[idx]
                    hline_val = param_values[key]
                    ax[idx].axhline(y = hline_val, linewidth = 2,
                                   linestyle = '--', color='black')
            return fig, ax
        else:
            print("Run mcmc before plotting")
            return None;

    def corner_plot(self,nburn=0,param_values=None,width=12,**kwargs):
        ndim = len(self.param_name_list)
        data = self.sampler.chain[:, nburn:,].reshape(-1, ndim)
        if param_values == None:
            truths = None;
        else:
            truths = []
            labels = []
            for key in self.param_name_list:
                truths.append(param_values[key])
                labels.append(key)
        print(labels)
        fig, ax = plt.subplots(np.shape(data)[1],
                               np.shape(data)[1],
                               figsize=(width,width))
        fig = corner.corner(data,
                            labels = self.param_name_list,
                            truths= truths,
                            truth_color = 'xkcd:bright orange',
                            show_titles = True,
                            use_math_text = True,
                            bins = 50,
                            #range = [(ll*A, ul*A), (ll*f, ul*f), (ll*tau, ul*tau)],
                            #levels=(0.95,),
                            color = 'xkcd:irish green',
                            smooth = 0.5, # smoothing scale in std's
                            hist_kwargs  = {'linewidth':2.5},
                            label_kwargs = {'fontsize':'large', 'fontweight':'bold'},
                            title_kwargs = {'fontsize':'medium', 'fontweight':'bold'},
                            fig = fig)
        return fig

def plot_walkers(self,steps_start=None,
                 steps_stop=None,
                 param_values= None,
                 *args,**kwargs):
    nrows = len(self.param_name_list)
    if self.plotting_ready:
        fig, ax = plt.subplots(nrows= nrows,
                              ncols= 1,
                              sharex= True, **kwargs)
        for idx in range(0, nrows):
            for walker in range(0,self.nwalkers):
                if steps_start == None:
                    steps_start = 0;
                if steps_stop == None:
                    steps_stop = self.nsteps;
                x_list = range(steps_start, steps_stop)
                y_list = list(self.sampler.chain[walker,
                                                 steps_start:steps_stop,
                                                 idx])
                ax[idx].plot(x_list, y_list);
                ax[idx].set_ylabel(self.param_name_list[idx])
            if not (param_values == None):
                key = self.param_name_list[idx]
                hline_val = param_values[key]
                ax[idx].axhline(y = hline_val, linewidth = 2,
                               linestyle = '--', color='black')
        return fig, ax
    else:
        print("Run mcmc before plotting")
        return None;

def corner_plot(self,nburn=0,param_values=None,width=12,**kwargs):
    ndim = len(self.param_name_list)
    data = self.sampler.chain[:, nburn:,].reshape(-1, ndim)
    if param_values == None:
        truths = None;
    else:
        truths = []
        labels = []
        for key in self.param_name_list:
            truths.append(param_values[key])
            labels.append(key)
    print(labels)
    fig, ax = plt.subplots(np.shape(data)[1],
                           np.shape(data)[1],
                           figsize=(width,width))
    fig = corner.corner(data,
                        labels = self.param_name_list,
                        truths= truths,
                        truth_color = 'xkcd:bright orange',
                        show_titles = True,
                        use_math_text = True,
                        bins = 50,
                        #range = [(ll*A, ul*A), (ll*f, ul*f), (ll*tau, ul*tau)],
                        #levels=(0.95,),
                        color = 'xkcd:irish green',
                        smooth = 0.5, # smoothing scale in std's
                        hist_kwargs  = {'linewidth':2.5},
                        label_kwargs = {'fontsize':'large', 'fontweight':'bold'},
                        title_kwargs = {'fontsize':'medium', 'fontweight':'bold'},
                        fig = fig)
    return fig
    #fig.set_figwidth(width)


class noise_asd2timeseries(object):
    '''
    Generate timeseries data from noise ASD
    
    This class converts given asd plot to time series. ASD input can be from 
    a csv file or it can be a array of tuples. This class has methods to plot
    asd and timeseries.
    
    Parameters
    
        data : can be a filename of csv file to read single sided ASD 
            csv structure shall be as follows
            First line is title
            Second line freq, noise
            all other lines contain single sided asd data
            <freq1>, <noise1>
            <freq2>, <noise2>
            or
            can be an array of tuples of data containing SS ASD
            in the following format 
            [(<freq1>, <noise1>), (<freq2>, <noise2>), ...]
            Note : noise is in units/sqrt(Hz)
        
        N_samples : no of samples to be generated in the time series
        Fs : sampling frequency
        external_file : set to True if the data is passed from csv file 
        fill_value: see document scipy.interpolate.interp1d
    '''
    
    def __init__(self, data,           #data in array of tuples
                 N_samples,            #No of samples in time series
                 Fs,                   #Sampling Frequency
                 external_file = False,#Whether data is from externalfile
                 fill_value = "extrapolate", # see document scipy.interpolate.interp1d
                 title = None,         #Title to set in plots
                 ylabel = None):        #Y label for plots
        '''
           Initialize class
       
        '''
        if external_file == True:
            noise_data = pd.read_csv(data, sep = ',',
                                     header=0, 
                                     names = ['freq', 'noise'],
                                     skiprows=0)
            freq_data = np.array(noise_data['freq'])
            noise_data = np.array(noise_data['noise'])
        else:
            freq_data = []
            noise_data = []
            for item in data:
                freq_data.append(item[0]);
                noise_data.append(item[1]);
        # generate the data in format
        freq_data = np.array(freq_data)
        noise_data = np.array(noise_data)
        
        self.freq_d = freq_data;
        self.noise_d = noise_data;
        
        #create interpolate function for generating FFT data from
        #ASD information given
        asd_interpolate = interp1d(np.log10(freq_data),
                                   20.0*np.log10(noise_data),
                                   kind= 'linear',
                                   bounds_error= False,
                                   fill_value= fill_value)
        Ts = 1.0/Fs;
        T = Ts*N_samples;
        F_start = 1.0/T;
        F_stop = Fs/2.0
        F_step = 1.0/T;
        self.freq = np.arange(F_start,F_stop, F_step)
        self.noise = np.power(10,asd_interpolate(np.log10(self.freq))/20.0)
        
        #Create FFT data from the Single Sided ASD data given
        fft_pos = np.concatenate((self.noise[0:1],
                                  self.noise/np.sqrt(2)))*float(N_samples)
        fft_neg = np.flipud(fft_pos)
        phase_pos = np.random.random(int(N_samples/2))*2.0*np.pi - np.pi
        phase_pos[0] = 0
        phase_neg = np.flipud(-phase_pos)
        phase_neg = np.random.random(int(N_samples/2))*2.0*np.pi - np.pi
        fft_pos_comp = fft_pos*np.exp(1j*phase_pos)
        fft_neg_comp = fft_neg*np.exp(1j*phase_neg)
        #ftt_neg_comp = (np.real(np.flipud(fft_pos_comp))-
        #                np.imag(np.flipud(fft_pos_comp)))
        self.fft_data = np.concatenate((fft_pos_comp,
                                        fft_neg_comp))
        self.timeseries_noise = np.real(np.fft.ifft(self.fft_data))
        self.timeseries_time = np.arange(0, T, Ts)
        self.ylabel = ylabel
        self.title = title
        return None
    
    def plot_ASD(self,*args, **kwargs):
        '''
        plot ASD in loglog scale. The data used in this plot is used 
        for the conversion to time series
        '''
        fig, ax = plt.subplots(figsize=(12,6), **kwargs)
        ax.loglog(self.freq, self.noise)
        ax.set_xlabel('Frequency')
        ax.set_ylabel(self.ylabel +'/sqrt(Hz)')
        ax.set_title(self.title)
        return fig, ax
    
    def plot_timeseries(self, *args, **kwargs):
        '''
        plot time series
        
        '''
        fig, ax = plt.subplots(figsize=(12,6), **kwargs)
        ax.plot(self.timeseries_time, self.timeseries_noise)
        ax.set_xlabel('time')
        ax.set_ylabel(self.ylabel)
        ax.set_title(self.title)
        return fig, ax