import numpy as np
global obname   # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global obname1  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global obname2  # global variable which can be accessed outside this module, useful in assigning it a pykat object 
global lnpriorlims
global globdata


def modelFunctionTFTPDHComp(theta):
    
    T1, T2, le, phi = theta
#    obname=Baseob.deepcopy()
    
    obname.ITMX.R = 1 - T1
    obname.ITMX.T = T1
    obname.ETMX.R = 1 - T2
    obname.ETMX.T = T2


    obname1.ITMX.R = 1 - T1
    obname1.ITMX.T = T1
    obname1.ETMX.R = 1 - T2
    obname1.ETMX.T = T2
    obname1.lp.L   = le
    #obname1.eo1.f=df
    #obname1.inphase.f1=df
    #obname1.REFLI.f1=df
    #obname1.REFLQ.f1=df
    obname1.REFL_I.phase1 = phi
    obname1.REFL_Q.phase1 = phi+90



    obname2.ITMX.R = 1 - T1
    obname2.ITMX.T = T1
    obname2.ETMX.R = 1 - T2
    obname2.ETMX.T = T2
    obname2.lp.L   = le
    #obname2.eo1.f=df
    #obname2.inphase.f1=df



    try:
        res  = obname.run()
        res1 = obname1.run()
        res2 = obname2.run()
        # print("te=",res['te'])
        # print(res2['my'].shape)
        # store as DICT instead
        data = {}
        data['P_trans']   = res['P_Trans']
        data['REFL_I']    = res1['REFL_I']
        data['REFL_Q']    = res1['REFL_Q']
        data['REFL_I_AC'] = res2['REFL_I_AC']

        return np.concatenate([res['P_Trans'], res1['REFL_I'], res1['REFL_Q'], res2['REFL_I_AC']])
        # return np.concatenate([res['P_Trans'], res1['REFL_I'], res1['REFL_Q'], res2['REFL_I_AC']]), data
        # return np.concatenate([res['P_Trans'], np.cos(phi)*res1['REFLI'] - np.sin(phi)*res1['REFLQ'] ,np.sin(phi)*res1['REFLI'] + np.cos(phi)*res1['REFLQ'] ,res2['inphase']])
        # return np.concatenate([res['P_Trans'], res1['inphase'] ])
    except Exception as e:
        print("An exception occurred")
        print(e)
        return np.inf*np.ones((1001,))


def lnlikeTFTPDHComp(theta):
    model = modelFunctionTFTPDHComp(theta)
    return -0.5 * np.sum(((globdata - model)**2)[:])    

def lnpriorTFTPDHComp(theta,lnpriorlims):
    T1, T2, le, ph = theta
    if lnpriorlims[0] < T1 < lnpriorlims[1] and  lnpriorlims[2] < T2 < lnpriorlims[3] and  lnpriorlims[4] < le < lnpriorlims[5] and  lnpriorlims[6] < ph < lnpriorlims[7] :
# 0.16000000000000003 0.24 and 0.48 0.72
    # if 0.16 <T1 < 0.24 and .48 < T2< .72 :
    # if 0.008 <T1 < 0.012 and 8.0e-06 < T2< 1.2e-05 :
        return 0.0
    return -np.inf  

def lnpostTFTPDHComp(theta):
    if not np.isfinite(lnpriorTFTPDHComp(theta,lnpriorlims)):
        return -np.inf
    return lnlikeTFTPDHComp(theta) + lnpriorTFTPDHComp(theta,lnpriorlims) 
