#  DRMi locking (limited by Arm and SRC finesse)
Status: ongoing.

## Overview
The SRC is tricky to lock because it has a low finesse (mode hopping, …). The choice of SRC finesse resulted from the (somewhat arbitrary, see <a href="https://dcc.ligo.org/LIGO-T070303">T070303</a>) choice of a relatively low finesse for the arm cavities, which was designed to aid lock acquisition but may have been overkill.  

Goal:  Increase the SRM and ITM reflectivities to get better, cleaner signals for SRC and arm locking so that the full DRMi can be locked more reliably and quickly.

### Knock-on effects for the ifo
 * Sensitivity curve & DARM pole changes: trade-off higher finesse arm and SRC with a lower PRG 
 * Control, mode matching, thermal effects, PIs, ...
 * ?
 
### Potential figures of merit
 * DRMi pole
 * SRC HOM scan 
 * SRC sensing error signal shape, e.g. mode hopping likelihood (specifics?? )
 * Sensitivity curve (QNLS)
 * (check) other LSC error signals
 * (check) ASC error signals
 * ?
 
### Useful refs
 * SRC finesse original design decision resoning: <a href="https://dcc.ligo.org/LIGO-T070303">T070303</a>
 * Paul’s mode hopping study (2015): Tech Note [<a href="https://dcc.ligo.org/LIGO-T1500230">T1500230</a>], Slides [<a href="https://dcc.ligo.org/LIGO-G1500645">G1500645</a>] → tilt to length coupling (mode hopping, lockloss from mode mixing, offsets to SRC tuning when mode matching changes,...)

 
 
## Files in this directory
* transmission_optimization (active): 
 exploratory notebook developing various tools that can be used to optimize mirror transmissions to meet a power or pole frequency requirement. Optimizing by iteratively zooming to the value / minimum difference, interpolating a curve, or a mix of both. Functions developed here evolve into those collected in the DRMiFinesse.py script.
* CavFinesseChanges (concluded): 
     (a) How does changing the finesse of each cavity change the sensitivity curve / DARM pole?
     (b) First application of the transmission optimization functions
     (c) First script fully optimizing the full IFO to match the original aLIGO sensitivity using a different ITM transmission (and correspondingly different PRM, SRM transmissions), showing how this affects the error signals for the linear DOFs when no misalignment/mismatch is included. Script worrks up to maxtem 0 and *usually* 2, but bugs out a bit at maxtem 4, which we want for the HOM effects. Ending this notebook here as a record and moving to use DRMiFinesse.py.
* DRMiFinesse.py (active):
  collection of functions developed for this study.
* CavFinesseChanges_2 (concluded):
  Replicates CavFinesseChanges, but using external functions in DRMiFinesse.py. Purpose was purely to check nothing broke in translating the tools over to the external script, but potentially easier to skim-read.
* 